package br.com.demo.controleponto.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.demo.controleponto.models.Usuario;


public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {
}

