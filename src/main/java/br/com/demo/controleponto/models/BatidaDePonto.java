package br.com.demo.controleponto.models;

import br.com.demo.controleponto.enums.TipoBatidaEnum;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class BatidaDePonto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private int id_usuario;

    private LocalDateTime horaDaBatida;

    @NotNull
    private TipoBatidaEnum tipoBatidaEnum;

    public BatidaDePonto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public LocalDateTime getHoraDaBatida() {
        return horaDaBatida;
    }

    public void setHoraBatida(LocalDateTime horaDaBatida) {
        this.horaDaBatida = horaDaBatida;
    }

    public TipoBatidaEnum getTipoDaBatida() {
        return tipoBatidaEnum;
    }

    public void setTipoBatida(TipoBatidaEnum tipoBatidaEnum) {
        this.tipoBatidaEnum = tipoBatidaEnum;
    }
}
