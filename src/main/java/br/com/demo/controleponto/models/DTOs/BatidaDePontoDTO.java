package br.com.demo.controleponto.models.DTOs;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class BatidaDePontoDTO {

    private LocalDateTime batidaEntrada;

    private LocalDateTime batidaSaida;

    private BigDecimal quantidadeHorasTrabalhadas;

    public BatidaDePontoDTO() {}

    public BatidaDePontoDTO(LocalDateTime batidaEntrada, LocalDateTime batidaSaida, BigDecimal horasTrabalhadas) {
        this.batidaEntrada = batidaEntrada;
        this.batidaSaida = batidaSaida;
        this.quantidadeHorasTrabalhadas = horasTrabalhadas;
    }

    public LocalDateTime getBatidaEntrada() {
        return batidaEntrada;
    }

    public void setBatidaEntrada(LocalDateTime batidaEntrada) {
        this.batidaEntrada = batidaEntrada;
    }

    public LocalDateTime getBatidaSaida() {
        return batidaSaida;
    }

    public void setBatidaSaida(LocalDateTime batidaSaida) {
        this.batidaSaida = batidaSaida;
    }

    public BigDecimal getQuantidadeHorasTrabalhadas() {
        return quantidadeHorasTrabalhadas;
    }

    public void setQuantidadeHorasTrabalhadas(BigDecimal quantidadeHorasTrabalhadas) {
        this.quantidadeHorasTrabalhadas = quantidadeHorasTrabalhadas;
    }
}
