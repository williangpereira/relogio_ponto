package br.com.demo.controleponto.services;

import br.com.demo.controleponto.enums.TipoBatidaEnum;
import br.com.demo.controleponto.models.DTOs.BatidaDePontoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.demo.controleponto.models.BatidaDePonto;
import br.com.demo.controleponto.models.Usuario;
import br.com.demo.controleponto.repositories.BatidaDePontoRepository;
import br.com.demo.controleponto.repositories.UsuarioRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class BatidaDePontoService {

    @Autowired
    private BatidaDePontoRepository batidaDePontoRepository;

    public BatidaDePonto criarBatidaDePonto(BatidaDePonto batidaDePonto){
        LocalDateTime hora = LocalDateTime.now();
        batidaDePonto.setHoraBatida(hora);
        BatidaDePonto batidaDePontoObjeto = batidaDePontoRepository.save(batidaDePonto);
        return batidaDePontoObjeto;
    }

    public BatidaDePontoDTO buscarBatidaPorUsuario(int id_usuario){
        LocalDateTime batidaEntrada = null;
        LocalDateTime batidaSaida = null;
        long horasTrabalhadas = 0;
        BatidaDePontoDTO batidaDePontoDTO = new BatidaDePontoDTO();

        Iterable<BatidaDePonto> batidaDePontos = batidaDePontoRepository.findAllById(id_usuario);

        for (BatidaDePonto batida : batidaDePontos) {
            if (batida.getTipoDaBatida() == TipoBatidaEnum.ENTRADA) {
                batidaEntrada = batida.getHoraDaBatida();
            } else {
                batidaSaida = batida.getHoraDaBatida();
            }
        }
        Duration duracao = Duration.between(batidaEntrada, batidaSaida);
        horasTrabalhadas = duracao.toMinutes();

        batidaDePontoDTO.setBatidaEntrada(batidaEntrada);
        batidaDePontoDTO.setBatidaSaida(batidaSaida);
        batidaDePontoDTO.setQuantidadeHorasTrabalhadas(new BigDecimal((double) horasTrabalhadas / 60).setScale(2, RoundingMode.HALF_UP));

        return batidaDePontoDTO;
    }
}
