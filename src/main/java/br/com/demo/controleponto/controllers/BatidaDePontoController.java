package br.com.demo.controleponto.controllers;


import br.com.demo.controleponto.models.BatidaDePonto;
import br.com.demo.controleponto.models.DTOs.BatidaDePontoDTO;
import br.com.demo.controleponto.services.BatidaDePontoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/pontos")
public class BatidaDePontoController {

    @Autowired
    BatidaDePontoService batidaDePontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BatidaDePonto cadastrarBatidaDePonto(@RequestBody @Valid BatidaDePonto batidaDePonto){
        BatidaDePonto batidaDePontoObjeto = batidaDePontoService.criarBatidaDePonto(batidaDePonto);
        return batidaDePontoObjeto;
    }

    @GetMapping("/{id_usuario}")
    public BatidaDePontoDTO bucarPorIdDdUsuario(@PathVariable(name = "id_usuario") int idUsuario){
        try{
            BatidaDePontoDTO batidaDePontos = batidaDePontoService.buscarBatidaPorUsuario(idUsuario);
            return batidaDePontos;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
