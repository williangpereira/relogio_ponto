package br.com.demo.controleponto.services;

import br.com.demo.controleponto.enums.TipoBatidaEnum;
import br.com.demo.controleponto.models.BatidaDePonto;
import br.com.demo.controleponto.repositories.BatidaDePontoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;

@SpringBootTest
public class BatidaDePontoServiceTest {

    @MockBean
    private BatidaDePontoRepository batidaDePontoRepository;

    @Autowired
    private BatidaDePontoService batidaDePontoService;

    private BatidaDePonto batidaDePonto;

    @BeforeEach
    public void setUp() {
        BatidaDePonto batidaDePonto = new BatidaDePonto();
        batidaDePonto.setId(1);
        batidaDePonto.setHoraBatida(LocalDateTime.now());
        batidaDePonto.setTipoBatida(TipoBatidaEnum.ENTRADA);

        BatidaDePonto batidaDePonto2 = new BatidaDePonto();
        batidaDePonto2.setId(2);
        batidaDePonto2.setHoraBatida(LocalDateTime.now());
        batidaDePonto2.setTipoBatida(TipoBatidaEnum.SAIDA);
    }

    @Test
    public void testarCriarBatidaDePonto() {
        BatidaDePonto batidaDePontoTeste = new BatidaDePonto();
        batidaDePontoTeste.setHoraBatida(LocalDateTime.now());
        batidaDePontoTeste.setTipoBatida(TipoBatidaEnum.ENTRADA);

        Mockito.when(batidaDePontoRepository.save(batidaDePontoTeste)).then(leadLamb -> {
            batidaDePontoTeste.setId(1);
            return batidaDePontoTeste;
        });
        BatidaDePonto batidaDePontoObjeto = batidaDePontoService.criarBatidaDePonto(batidaDePontoTeste);

        Assertions.assertEquals(batidaDePontoObjeto, batidaDePontoTeste);
    }
}

