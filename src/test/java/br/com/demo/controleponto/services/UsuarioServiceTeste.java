package br.com.demo.controleponto.services;

import br.com.demo.controleponto.models.Usuario;
import br.com.demo.controleponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class UsuarioServiceTeste {

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    Usuario usuario;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setNome("Willian");
        usuario.setCpf("38229179824");
        usuario.setEmail("willian@teste.com");
    }

    @Test
    public void testarCriarUsuario(){

        Usuario usuarioTeste = new Usuario();
        usuarioTeste.setNome("teste");
        usuarioTeste.setCpf("38229179824");
        usuarioTeste.setEmail("teste@teste.com");

        Mockito.when(usuarioRepository.save(usuarioTeste)).then(lamb -> {
            usuarioTeste.setCpf("38229179824");
            return usuarioTeste;
        });

        Usuario usuarioObjeto = usuarioService.cadastrarUsuario(usuarioTeste);

        Assertions.assertEquals("38229179824", usuarioTeste.getCpf());
    }


//    @Test
//    public void testarBuscarClientePorEmail(){
//
//        Mockito.when(usuarioService.buscarPorCPF(Mockito.anyString())).thenReturn(usuario);
//        Usuario usuarioPorCPF = usuarioService.buscarPorCPF("38229179824");
//        Assertions.assertEquals(usuarioPorCPF, usuario);
//    }


}
