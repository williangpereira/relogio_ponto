package br.com.demo.controleponto.controllers;

import br.com.demo.controleponto.enums.TipoBatidaEnum;
import br.com.demo.controleponto.models.BatidaDePonto;
import br.com.demo.controleponto.models.Usuario;
import br.com.demo.controleponto.services.BatidaDePontoService;
import br.com.demo.controleponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDateTime;

@WebMvcTest(BatidaDePontoController.class)
public class BatidaDePontoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BatidaDePontoService batidaDePontoService;

    BatidaDePonto batidaDePonto;

    @Test
    public void testarCadastrarBatidaComSucesso() throws Exception {
        BatidaDePonto batidaDePonto = new BatidaDePonto();
        batidaDePonto.setId(1);
        batidaDePonto.setId_usuario(1);
        batidaDePonto.setTipoBatida(TipoBatidaEnum.ENTRADA);

        Mockito.when(batidaDePontoService.criarBatidaDePonto(Mockito.any(BatidaDePonto.class))).then(batidaDePontoObjeto -> {
            batidaDePonto.setHoraBatida(LocalDateTime.now());
            return batidaDePonto;
        });


        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(batidaDePonto);

        mockMvc.perform(MockMvcRequestBuilders.post("/pontos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

}
